// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyDKwA_DIoLZ8FmxaZMxa6UqoJVLmJUmAwE",
    authDomain: "dpad-mydrtv.firebaseapp.com",
    databaseURL: "https://dpad-mydrtv.firebaseio.com",
    projectId: "dpad-mydrtv",
    storageBucket: "dpad-mydrtv.appspot.com",
    messagingSenderId: "402626031747",
    appId: "1:402626031747:web:09faaddb47719bf8"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
